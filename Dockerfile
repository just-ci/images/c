FROM python:alpine

ENV PIP_ROOT_USER_ACTION=ignore
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

RUN apk add --no-cache cppcheck

RUN pip install --no-cache-dir --upgrade pip flawfinder cppcheck-junit

ENTRYPOINT [""]
